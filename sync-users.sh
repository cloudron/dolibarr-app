#!/bin/bash
set -eu

echo "==> Syncing LDAP users into dolibarr"
/usr/bin/php /app/code/scripts/user/sync_users_ldap2dolibarr.php -y || true
