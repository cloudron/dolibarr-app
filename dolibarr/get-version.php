<?php
/*
 * Copyright (C) 2021  Éric Seigne <eric.seigne@informatique-libre.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *	\file       htdocs/install/get-version.php
 *	\ingroup    install
 *	\brief      display current dolibarr version from database
 */
include 'inc.php';

$db = getDoliDBInstance($conf->db->type, $conf->db->host, $conf->db->user, $conf->db->pass, $conf->db->name, $conf->db->port);

$conf->setValues($db);

if (isset($conf->global->MAIN_VERSION_LAST_UPGRADE))
	print $conf->global->MAIN_VERSION_LAST_UPGRADE;
else
	print $conf->global->MAIN_VERSION_LAST_INSTALL;
