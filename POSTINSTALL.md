This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme123<br/>
**Email**: admin@cloudron.local<br/>

Please change the admin password and email immediately.

Users from Cloudron have to be synced into Dolibarr manually by running `sync-users.sh` in a webterminal into the app,
those can use Dolibarr but the pre-setup admin account has to be used to delegate rights to those users.
