[0.1.0]
* initial version

[0.1.1]
* preconfiguration

[0.1.4]
* new file install_upgrade.php wich returns http header code 425 ("Too Early") while database is not ready
* first step for tests

[0.1.5]
* change default language (auto-detect + multilang support)

[0.1.6]
* change mail settings from cloudron env vars
* clean up directories -> dolibarr folder with stuff inside

[0.1.7]
* LDAP configuration is working

[0.1.8]
* custom dir is now in data storage (users can upload custom modules "plugins")

[0.2.0]
* clean up base system : remove all old versions of PHP engine

[0.2.1]
* fix #1: Cron Jobs arent running (https://git.cloudron.io/cloudron/dolibarr-app/-/issues/1)

[0.2.2]
* change LDAP mapping (givenName -> sn)
* Update to base image v3

[0.3.0]
* upgrade to dolibarr 13.0.1

[1.0.0]
* Stable release with Dolibarr 13.0.1

[1.0.1]
* Update Dolibarr to 13.0.2
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/13.0.2/ChangeLog)

[1.0.2]
* Update Dolibarr to 13.0.3
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/13.0.3/ChangeLog)

[1.0.3]
* Update Dolibarr to 13.0.4
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/13.0.4/ChangeLog)

[1.1.0]
* Update Dolibarr to 14.0.0
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/14.0.0/ChangeLog)

[1.1.1]
* Update Dolibarr to 14.0.1
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/14.0.1/ChangeLog)

[1.1.2]
* Update Dolibarr to 14.0.2
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/14.0.2/ChangeLog)

[1.1.3]
* Update Dolibarr to 14.0.3
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/14.0.3/ChangeLog)

[1.1.4]
* Update Dolibarr to 14.0.4
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/14.0.4/ChangeLog)

[1.1.5]
* Update Dolibarr to 14.0.5
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/14.0.5/ChangeLog)

[1.2.0]
* Update Dolibarr to 15.0.0
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/15.0.0/ChangeLog)

[1.2.1]
* Update Dolibarr to 15.0.1
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/15.0.1/ChangeLog)

[1.2.2]
* Update Dolibarr to 15.0.2
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/15.0.2/ChangeLog)

[1.2.3]
* Update Dolibarr to 15.0.3
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/15.0.3/ChangeLog)
* Make LDAP login work

[1.3.0]
* Update Dolibarr to 16.0.0
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/16.0.0/ChangeLog)

[1.3.1]
* Update Dolibarr to 16.0.1
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/16.0.1/ChangeLog)
* Enable multiDomain flag to support serving up on multiple domains

[1.3.2]
* Update Dolibarr to 16.0.2
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/16.0.2/ChangeLog)

[1.3.3]
* Update Dolibarr to 16.0.3
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/16.0.3/ChangeLog)

[1.4.0]
* Update Dolibarr to 17.0.0
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/17.0.0/ChangeLog)

[1.5.0]
* Update base image to 4.0.0
* This updates PHP to 8.1, please check plugin compatibility

[1.5.1]
* Update Dolibarr to 17.0.1
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/17.0.1/ChangeLog)

[1.5.2]
* Update Dolibarr to 17.0.2
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/17.0.2/ChangeLog)

[1.5.3]
* Update Dolibarr to 17.0.3
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/17.0.3/ChangeLog)

[1.6.0]
* Update Dolibarr to 18.0.0
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/18.0.0/ChangeLog)

[1.6.1]
* Update Dolibarr to 18.0.1
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/18.0.1/ChangeLog)

[1.6.2]
* Update Dolibarr to 18.0.2
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/18.0.2/ChangeLog)

[1.6.3]
* Update Dolibarr to 18.0.3
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/18.0.3/ChangeLog)

[1.6.4]
* Update Dolibarr to 18.0.4
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/18.0.4/ChangeLog)

[1.6.5]
* Allow custom plugin installation

[1.6.6]
* Update Dolibarr to 18.0.5
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/18.0.5/ChangeLog)

[1.7.0]
* Update Dolibarr to 19.0.0
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/19.0.0/ChangeLog)

[1.7.1]
* Update Dolibarr to 19.0.1
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/19.0.1/ChangeLog)

[1.7.2]
* Update Dolibarr to 19.0.2
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/19.0.2/ChangeLog)

[1.7.3]
* Fix permission issue and various security warnings

[1.7.4]
* Update Dolibarr to 19.0.3
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/19.0.3/ChangeLog)

[1.8.0]
* Update Dolibarr to 20.0.0
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/20.0.0/ChangeLog)

[1.8.1]
* Update Dolibarr to 20.0.1
* [Changelog](https://github.com/Dolibarr/dolibarr/blob/20.0.1/ChangeLog)

[1.8.2]
* Fix warnings on on user syncing

[1.8.3]
* Update dolibarr to 20.0.2
* [Full Changelog](https://github.com/Dolibarr/dolibarr/blob/20.0.2/ChangeLog)

[1.8.4]
* Update dolibarr to 20.0.3
* [Full Changelog](https://github.com/Dolibarr/dolibarr/blob/20.0.3/ChangeLog)

[1.8.5]
* Update dolibarr to 20.0.4

[1.9.0]
* Update dolibarr to 21.0.0
* [Full Changelog](https://github.com/Dolibarr/dolibarr/blob/21.0.0/ChangeLog)
* Prepare your module for deprecation of triggers code XXX_INSERT to support also XXX_CREATE.
* More class properties (with old name in french) are now deprecated in favor of the property name in english.
* The json emulator dol_json_encode/decode() is removed. The native json PHP module must be enabled/available (this is the case by default with most PHP installation).
* The deprecated GET parameter "\&sall=" has been removed, use now the "\&search_all=".
* The experimental and deprecated module WebserviceClient is completely removed (was never released and use deprecated architecture). It may be replaced with the stable module Webhook.
* The dynamic properties ->no_button_delete, ->no_button_edit, ->no_button_copy for $object Product that could be set by an external module must no more be
* The old function dol_bc($var, $moreclass = '') has been removed. If you called it, just stop to call it (the function has no effect since a long time).
* The trigger code CATEGORY_LINK and CATEGORY_UNLINK have been replaced with code CATEGORY_MODIFY. You can read ->context\['linkto'] or ->context\['unlinkoff'] to detect if we want to make a link or unlink.
* The property ->domiciliation and ->propio on bank accounts has been deprecated and replaced with property ->address and ->owner_name everywhere.
* If you were using the substitution key **MEMBER_CIVILITY**, you must now use **MEMBER_TITLE**
* The hidden title of tab that was hidden by the CSS class tabTitleText has been completely removed from HTML content.

