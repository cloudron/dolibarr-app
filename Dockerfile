FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# required packages
RUN apt-get -y update && apt-get install -y geoip-database && rm -rf /var/cache/apt /var/lib/apt/lists

# Configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/dolibarr.conf /etc/apache2/sites-enabled/dolibarr.conf
RUN a2enmod rewrite headers expires deflate mime dir rewrite setenvif

# configure mod_php
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 512M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP allow_url_fopen 0 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.use_strict_mode 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/dolibarr/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN rm -rf /var/lib/php/sessions && ln -s /run/php/sessions /var/lib/php/sessions

RUN cp /etc/php/8.1/apache2/php.ini /etc/php/8.1/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

# renovate: datasource=github-releases depName=Dolibarr/dolibarr versioning=semver
ENV DOLIBARR_VERSION=21.0.0
RUN curl -L https://github.com/Dolibarr/dolibarr/archive/${DOLIBARR_VERSION}.tar.gz | tar zx --strip-components 1 -C /app/code

# config file
RUN mv /app/code/htdocs/conf/conf.php.example /app/pkg/conf.php.example
COPY dolibarr/cloudron.conf.php /app/pkg/cloudron.conf.php
COPY dolibarr/conf.php /app/pkg/conf.php

COPY dolibarr/install.forced.php /app/code/htdocs/install/install.forced.php

# special cron file without secure key
COPY dolibarr/cron_run_jobs_custom.php /app/pkg/cron_run_jobs_custom.php

# get dolibarr version
COPY dolibarr/get-version.php /app/code/htdocs/install/get-version.php

RUN rm -rf /app/code/htdocs/conf && ln -s /run/dolibarr/conf /app/code/htdocs/conf && \
    rm -rf /app/code/htdocs/custom && ln -s /app/data/custom /app/code/htdocs/custom

COPY sync-users.sh /usr/sbin/sync-users.sh

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
