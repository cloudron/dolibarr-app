<?php

$dolibarr_main_url_root=getenv('CLOUDRON_APP_ORIGIN');
$dolibarr_main_document_root='/app/code/htdocs';
$dolibarr_main_data_root='/app/data/dolibarr';
$dolibarr_main_document_root_alt='/app/data/custom';

// Database configs
$dolibarr_main_db_host='mysql';
$dolibarr_main_db_port=getenv('CLOUDRON_MYSQL_PORT');
$dolibarr_main_db_name=getenv('CLOUDRON_MYSQL_DATABASE');
$dolibarr_main_db_prefix='dolibarr_';
$dolibarr_main_db_user=getenv('CLOUDRON_MYSQL_USERNAME');
$dolibarr_main_db_pass=getenv('CLOUDRON_MYSQL_PASSWORD');
$dolibarr_main_db_type='mysqli';
$dolibarr_main_db_character_set='utf8';
$dolibarr_main_db_collation='utf8_unicode_ci';

// LDAP configs
// This is only for authentication. User records are synced with the LDAP module and configured in the database
$dolibarr_main_authentication='ldap,dolibarr';   // Permet de garder la double authentification si souci avec LDAP (gardez un compte non LDAP admin !)
$dolibarr_main_auth_ldap_host=getenv('CLOUDRON_LDAP_SERVER');  // Plusieurs serveurs peuvent être ajouter en séparant par une virgule.
$dolibarr_main_auth_ldap_port=getenv('CLOUDRON_LDAP_PORT');    // Port
$dolibarr_main_auth_ldap_version='3';
$dolibarr_main_auth_ldap_servertype='openldap';  // openldap, activedirectory or egroupware
$dolibarr_main_auth_ldap_login_attribute='uid';  // Ex: uid or samaccountname for active directory
$dolibarr_main_auth_ldap_dn=getenv('CLOUDRON_LDAP_USERS_BASE_DN'); // Ex: ou=users,dc=my-domain,dc=com
$dolibarr_main_auth_ldap_filter='username=%1%';            // If defined, two previous parameters are not used to find a user into LDAP. Ex: (uid=%1%) or &(uid=%1%)(isMemberOf=$
$dolibarr_main_auth_ldap_admin_login=getenv('CLOUDRON_LDAP_BIND_DN');
$dolibarr_main_auth_ldap_admin_pass=getenv('CLOUDRON_LDAP_BIND_PASSWORD');
$dolibarr_main_auth_ldap_debug='false';

$dolibarr_main_prod='1';

