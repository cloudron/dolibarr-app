#!/bin/bash
set -eu

echo "==> Starting Dolibarr"

APACHE_PID_FILE="/var/run/apache2/apache2.pid"

mkdir -p /run/dolibarr/{sessions,upgrade-logs,conf} /app/data/{dolibarr,conf,custom} /run/php/sessions

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

firstTimeSetup() {
    while [[ ! -f "${APACHE_PID_FILE}" ]]; do
        echo "==> Waiting for apache to start"
        sleep 3
    done

    echo "==> Fresh installation, performing Dolibarr first time setup"

    cp /app/pkg/cloudron.conf.php /run/dolibarr/conf/conf.php
    chown -R www-data:www-data /app/data /run/dolibarr

    echo "==> Running install steps"
    LOCALURI="http://localhost:8000/install"
    echo "===> Install step N°1 ... "
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/fileconf.php > /dev/null 2>&1 # this will already generate some /run/dolibarr/conf/conf.php 
    echo "===> Install step N°2 ... "
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/step1.php --data "testpost=ok&action=set&selectlang=en_EN" > /dev/null 2>&1
    echo "===> Install step N°3: create database (could take a lot of time) ... "
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/step2.php --data "testpost=ok&action=set&dolibarr_main_db_character_set=latin1&dolibarr_main_db_collation=latin1_swedish_ci&selectlang=en_EN" > /dev/null 2>&1
    echo "===> Install step N°4 ..."
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/step4.php --data "testpost=ok&action=set&selectlang=en_EN" > /dev/null 2>&1
    echo "===> Install step N°5 ... creating account (default is admin / changeme123) ..."
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/step5.php --data "testpost=ok&action=set&selectlang=en_EN&pass=changeme123&pass_verif=changeme123" > /dev/null 2>&1

    # stash the resulting config file into app data
    echo "==> Copying generated conf.php to /app/data/conf/conf.php"
    cp /run/dolibarr/conf/conf.php /app/data/conf/conf.php

    touch /app/data/dolibarr/install.lock # install lock
    chown -R root:root /run/dolibarr/conf # fix ownership, otherwise it complains
}

setup() {
    cp /app/pkg/conf.php /run/dolibarr/conf/conf.php # conf file that includes install generated conf file and our custom conf
    chown -R root:root /run/dolibarr/conf # fix ownership, otherwise it complains

    # starting upgrade
    cd /app/code/htdocs/install
    [[ -f /app/data/dolibarr/install.lock ]] && mv /app/data/dolibarr/install.lock /tmp/install.lock # otherwise, we cannot run the script below
    PREV_DOLIBARR_VERSION=`php /app/code/htdocs/install/get-version.php`
    echo "==> Running upgrade hooks . ${PREV_DOLIBARR_VERSION} -> ${DOLIBARR_VERSION}"

    if [[ "${PREV_DOLIBARR_VERSION}" != "${DOLIBARR_VERSION}" ]]; then
        echo "==> Upgrading from version ${PREV_DOLIBARR_VERSION} to version ${DOLIBARR_VERSION}"
        php /app/code/htdocs/install/upgrade.php ${PREV_DOLIBARR_VERSION} ${DOLIBARR_VERSION} > /run/dolibarr/upgrade-logs/upgrade-01.html
        echo "===> Upgrade step N°1 ... done"
        php /app/code/htdocs/install/upgrade2.php ${PREV_DOLIBARR_VERSION} ${DOLIBARR_VERSION} > /run/dolibarr/upgrade-logs/upgrade-02.html
        echo "===> Upgrade step N°2 ... done"
        php /app/code/htdocs/install/step5.php ${PREV_DOLIBARR_VERSION} ${DOLIBARR_VERSION} > /run/dolibarr/upgrade-logs/upgrade-03.html
        echo "===> Upgrade step N°3 ... done"
    else
        echo "==> Version is unchanged, skipping upgrade"
    fi

    [[ -f /tmp/install.lock ]] && mv /tmp/install.lock /app/data/dolibarr/install.lock

    echo "==> Ensure email configuration"
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "\
        REPLACE INTO dolibarr_const (name, value, type) VALUES \
            ('MAIN_MAIL_EMAIL_STARTTLS', '0', 'chaine'), \
            ('MAIN_MAIL_EMAIL_TLS', '0', 'chaine'), \
            ('MAIN_MAIL_SENDMODE', 'smtps', 'chaine'), \
            ('MAIN_MAIL_DEFAULT_FROMTYPE', 'user', 'chaine'), \
            ('MAIN_MAIL_SMTP_SERVER', '${CLOUDRON_MAIL_SMTP_SERVER}', 'chaine'), \
            ('MAIN_MAIL_EMAIL_FROM', '${CLOUDRON_MAIL_FROM}', 'chaine'), \
            ('MAIN_MAIL_SMTPS_ID', '${CLOUDRON_MAIL_SMTP_USERNAME}', 'chaine'), \
            ('MAIN_MAIL_SMTPS_PW', '${CLOUDRON_MAIL_SMTP_PASSWORD}', 'chaine'), \
            ('MAIN_MAIL_SMTP_PORT','${CLOUDRON_MAIL_SMTP_PORT}','chaine');"

    echo "==> Ensure LDAP configuration"
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "\
        REPLACE INTO dolibarr_const (name, value, type) VALUES \
            ('LDAP_SERVER_TYPE', 'openldap', 'chaine'), \
            ('LDAP_SERVER_PROTOCOLVERSION', '3', 'chaine'), \
            ('LDAP_SERVER_HOST', '${CLOUDRON_LDAP_SERVER}', 'chaine'), \
            ('LDAP_SERVER_PORT', '${CLOUDRON_LDAP_PORT}', 'chaine'), \
            ('LDAP_SERVER_DN', '${CLOUDRON_LDAP_USERS_BASE_DN}', 'chaine'), \
            ('LDAP_USER_DN', '${CLOUDRON_LDAP_USERS_BASE_DN}', 'chaine'), \
            ('LDAP_ADMIN_DN', '${CLOUDRON_LDAP_BIND_DN}', 'chaine'), \
            ('LDAP_ADMIN_PASS', '${CLOUDRON_LDAP_BIND_PASSWORD}', 'chaine'), \
            ('LDAP_USER_OBJECT_CLASS', 'top', 'chaine'), \
            ('LDAP_FILTER_CONNECTION', '&(objectclass=user)', 'chaine'), \
            ('LDAP_SYNCHRO_ACTIVE', 'ldap2dolibarr', 'chaine'), \
            ('LDAP_FIELD_LOGIN','username','chaine'), \
            ('LDAP_FIELD_LOGIN_SAMBA','username','chaine'), \
            ('LDAP_FIELD_NAME','sn','chaine'), \
            ('LDAP_FIELD_MAIL','mail','chaine'), \
            ('LDAP_FIELD_FIRSTNAME','givenName','chaine');"
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "\
        REPLACE INTO dolibarr_const (name, value, type) VALUES \
            ('LDAP_FIELD_PASSWORD', '', 'chaine'), \
            ('LDAP_FIELD_PASSWORD_CRYPTED', '', 'chaine'), \
            ('LDAP_FIELD_PHONE', '', 'chaine'), \
            ('LDAP_FIELD_FAX', '', 'chaine'), \
            ('LDAP_FIELD_MOBILE', '', 'chaine'), \
            ('LDAP_FIELD_TITLE', '', 'chaine'), \
            ('LDAP_FIELD_DESCRIPTION', '', 'chaine'), \
            ('LDAP_FIELD_SID', '', 'chaine');"
}

echo "==> Ensure permissions"
chown -R www-data:www-data /app/data /run/dolibarr

if [[ ! -f /app/data/conf/conf.php ]]; then
    ( firstTimeSetup; setup ) &
else
    ( setup ) &
fi

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

