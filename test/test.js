#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const ADMIN_USERNAME = 'admin';
    const ADMIN_PASSWORD = 'changeme123';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function welcomePage() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//*[contains(text(), "Dolibarr")]'));
    }

    async function checkDash() {
        await browser.get('https://' + app.fqdn + '/');
        await waitForElement(By.xpath('//*[@id="topmenu-login-dropdown"]/a/span[contains(text(), "admin")]'));
    }

    async function login(username, password) {
        browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@type="submit"]')).click();
        await waitForElement(By.id('id-left'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);

        await waitForElement(By.id('topmenu-login-dropdown'));
        await browser.findElement(By.id('topmenu-login-dropdown')).click();
        await waitForElement(By.xpath('//a[contains(@title,"Logout")]'));
        await browser.findElement(By.xpath('//a[contains(@title,"Logout")]')).click();

        await waitForElement(By.id('username'));
    }

    async function syncLDAPUsers() {
        execSync(`cloudron exec --app ${app.id} sync-users.sh`);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(60000);
    });

    it('can get app information', getAppInfo);
    it('can view welcome page', welcomePage);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can access dashboard', checkDash);
    it('can logout', logout);

    it('can sync ldap users', syncLDAPUsers);
    it('can login with LDAP user', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });

    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can access dashboard', checkDash);
    it('can logout', logout);
    it('can login with LDAP user', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        await browser.manage().deleteAllCookies();
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        getAppInfo();
        await browser.sleep(10000);
    });

    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can access dashboard', checkDash);
    it('can logout', logout);
    it('can login with LDAP user', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });

    // test update
    it('can install app', async function () {
        execSync(`cloudron install --appstore-id org.dolibarr.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(60000);
    });
    it('can get app information', getAppInfo);

    it('can view welcome page', welcomePage);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can access dashboard', checkDash);

    it('can update', async function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can access dashboard', checkDash);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
